# Code Description

- ClientController is the class that describes endpoints and documents them using swagger (OpenApi). Also sometimes symfony controllers are used to specify user's role to run method with.
- ClientControllerTest is an integration test. The library called FactoryGirl is used to set up data to database.
In the test REST methods are run and then the test checks the answer for correctness.
- ClientPublicRegistration is a constraint that is used along with the symfony validator component
- ClientPublicRegistrationValidator checks the request data to be correct according to the business logic
- UserTransformer is a term that is related to the library called Fractal (https://packagist.org/packages/league/fractal)
It returns json data in JSON-API format. 
