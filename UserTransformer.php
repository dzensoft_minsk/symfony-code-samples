<?php
namespace App\Transformer;

use App\Entity\Company;
use App\Entity\Department;
use App\Entity\FrontendNotification;
use App\Entity\User;
use App\Repository\FrontendNotificationRepository;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;

class UserTransformer extends BaseTransformer
{
    use TransformerFactoryAwareTrait;

    /**
     * @var array
     */
    protected $availableIncludes = [
        'company',
        'department',
    ];

    protected $defaultIncludes = [
        'unread_notifications_count'
    ];

    /**
     * @var UserService $userService
     */
    private $userService;

    private $em;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService, EntityManagerInterface $em)
    {
        $this->userService = $userService;
        $this->em = $em;
    }

    /**
     * @param User $user
     * @return array
     *
     * @throws \Exception
     */
    public function transform(User $user)
    {
        $result = [
            'id' => $user->getId(),
            'username' => $this->safeOutput($user->getUsername()),
            'email' => $this->safeOutput($user->getEmail()),
            'first_name' => $this->safeOutput($user->getFirstName()),
            'last_name' => $this->safeOutput($user->getLastName()),
            'access_groups' => $this->userService->getSites($user),
            'enabled' => (int)$user->getEnabled(),
        ];

        return $result;
    }

    /**
     * @return \League\Fractal\Resource\Item
     */
    public function includeCompany(User $user)
    {
        $company = $this->loadIfDoctrineProxy($user->getCompany());
        if (!$company) {
            return null;
        }
        return $this->item(
            $company,
            $this->transformerFactory->create(CompanyTransformer::class),
            Company::JSON_API_TYPE
        );
    }

    /**
     * @return \League\Fractal\Resource\Item
     */
    public function includeDepartment(User $user)
    {
        $department = $this->loadIfDoctrineProxy($user->getDepartment());
        if (!$department) {
            return null;
        }

        return $this->item(
            $department,
            $this->transformerFactory->create(DepartmentTransformer::class),
            Department::JSON_API_TYPE
        );
    }

    public function includeUnreadNotificationsCount(User $user)
    {
        if (!$user->isStudent() && !$user->isClient()) {
            return;
        }

        /** @var FrontendNotificationRepository $notificationRepository */
        $notificationRepository = $this->em->getRepository(FrontendNotification::class);

        return $this->primitive($this->safeOutput($notificationRepository->getUnreadCount($user)));
    }
}
