<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\User;
use App\Event\EventList;
use App\Event\UserActivated;
use App\Event\UserCreated;
use App\Exception\ValidationFailedException;
use App\Filter\ClientFilter;
use App\Repository\DepartmentRepository;
use App\Repository\UserRepository;
use App\Service\Controller\QueryValidationInterface;
use App\Service\Fractal\Manager;
use App\Service\InviteService;
use App\Service\Mapper\JmsMappingContextFactory;
use App\Service\Mapper\PropertyMapper;
use App\Service\Pagination\ResourcePaginatorBuilder;
use App\Service\UserService;
use App\Sorter\Parser;
use App\Transformer\ClientTransformer;
use App\Validator\Constraint\ClientPublicRegistration;
use Doctrine\ORM\EntityRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\UserBundle\Model\UserManagerInterface;
use JMS\Serializer\SerializerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Swagger\Annotations as SWG;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClientController extends FOSRestController implements QueryValidationInterface
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var ResourcePaginatorBuilder
     */
    private $paginatorBuilder;

    private $serializer;

    private $eventDispatcher;

    /**
     * @param UserManagerInterface $userManager
     * @param ResourcePaginatorBuilder $paginatorBuilder
     * @param UserInterface $systemUser
     */
    public function __construct(
        UserManagerInterface $userManager,
        ResourcePaginatorBuilder $paginatorBuilder,
        SerializerInterface $serializer,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->userManager = $userManager;
        $this->paginatorBuilder = $paginatorBuilder;
        $this->serializer = $serializer;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Rest\Get("/clients/{id}")
     *
     * @SWG\Get(
     *      summary="Get The Client",
     *      tags={"Client"},
     *      @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="integer",
     *          required=true
     *      ),
     *      @SWG\Parameter(
     *          name="include",
     *          in="query",
     *          type="string",
     *          description="available values: company,department"
     *      ),
     *      @SWG\Response(
     *          response="200",
     *          description="Returned when successful"
     *      )
     * )
     * @param $id
     * @param Request $request
     * @param ClientTransformer $clientTransformer
     * @param Manager $fractalManager
     *
     * @return array
     */
    public function getClient(
        $id,
        Request $request,
        ClientTransformer $clientTransformer,
        Manager $fractalManager
    ) {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getManager()->getRepository('App:User');

        /** @var User $user */
        $client = $userRepository->getClient($id);
        if (!$client) {
            throw new NotFoundHttpException('Client is not found');
        }

        $include = $request->query->get('include', '');
        $fractalManager->parseIncludes($include);

        $resource = new \League\Fractal\Resource\Item(
            $client,
            $clientTransformer,
            User::JSON_API_TYPE_CLIENT
        );

        return $fractalManager->createData($resource)->toArray();
    }

    /**
     * @Rest\Get("/clients", name="api_get_clients")
     *
     * @SWG\Get(
     *      summary="Get List of Clients",
     *      tags={"Client"},
     *      @SWG\Parameter(
     *          name="page",
     *          in="query",
     *          type="integer",
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          in="query",
     *          type="integer",
     *      ),
     *      @SWG\Parameter(
     *          name="include",
     *          in="query",
     *          type="string",
     *          description="available values: company,department"
     *      ),
     *      @SWG\Parameter(
     *          name="last_name[like]",
     *          in="query",
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="first_name[like]",
     *          in="query",
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="email[like]",
     *          in="query",
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="company_name[like]",
     *          in="query",
     *          type="string",
     *          description=""
     *      ),
     *      @SWG\Parameter(
     *          name="enabled",
     *          in="query",
     *          type="integer",
     *          description="0|1"
     *      ),
     *      @SWG\Parameter(
     *          name="industry_ids",
     *          in="query",
     *          type="string",
     *          description="Comma-separated list of ids"
     *      ),
     *      @SWG\Response(
     *          response="200",
     *          description="Returned when successful",
     *      )
     * )
     * @param Request $request
     * @param ClientTransformer $clientTransformer
     * @param Manager $fractalManager
     *
     * @return array
     */
    public function getClients(
        Request $request,
        ClientTransformer $clientTransformer,
        Manager $fractalManager,
        ClientFilter $filter,
        Parser $sortingParamParser
    ) {
        $limit = $request->query->get('limit', 10);
        $page = $request->query->get('page', 1);
        $sort = $request->query->get('sort', $this->getDefaultSort());
        $include = $request->query->get('include', '');

        $fractalManager->parseIncludes($include);
        $entityManager = $this->getDoctrine()->getManager();

        /** @var EntityRepository $repository */
        $repository = $entityManager->getRepository('App:User');
        $qb = $repository->createQueryBuilder('t');
        $filter->apply($qb, $request, $sortingParamParser->parse($sort, $this->getSortingFields()));

        $paginator = $this
            ->paginatorBuilder
            ->setQuery($qb->getQuery())
            ->setLimit($limit)
            ->setPage($page)
            ->build();

        $resource = new \League\Fractal\Resource\Collection(
            $paginator->getCurrentPageResults(),
            $clientTransformer,
            User::JSON_API_TYPE_CLIENT
        );

        $resource->setPaginator($paginator);

        return $fractalManager->createData($resource)->toArray();
    }

    /**
     * @Rest\Get("/statistics")
     *
     * @SWG\Get(
     *      summary="Get The Statistics",
     *      tags={"Client"},
     *      @SWG\Response(
     *          response="200",
     *          description="Returned when successful"
     *      )
     * )
     *
     * @param User $systemUser
     * @param UserService $userService
     *
     * @return array
     */
    public function getStatistics(
        UserInterface $systemUser,
        UserService $userService
    ) {
        // :TODO is it used any longer?
        if ($systemUser->getIsStudent()) {
            throw new AccessDeniedHttpException('Access denied');
        }

        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getManager()->getRepository('App:User');
        $statistics = $userRepository->getCourseStatistics($systemUser->getCompany());

        return $userService->showStatistics($statistics);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @Rest\Post("/clients")
     *
     * @SWG\Post(
     *      summary="Create A New Client",
     *      description="<b>Field department_id</b>: can be null or a number",
     *      tags={"Client"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          @Model(type=App\Entity\User::class, groups={"client.post"})
     *      ),
     *      @SWG\Response(
     *          response="200",
     *          description="Returned when successful"
     *      )
     * )
     *
     * @throws ValidationFailedException
     * @throws \Exception
     *
     * @param User $client
     * @param User $systemUser
     * @param ConstraintViolationListInterface $validationErrors
     * @param ClientTransformer $clientTransformer
     * @param Manager $fractalManager
     *
     * @return array
     */
    public function postClient(
        Request $request,
        JmsMappingContextFactory $mappingContextFactory,
        PropertyMapper $propertyMapper,
        InviteService $inviteService,
        UserInterface $systemUser,
        ClientTransformer $clientTransformer,
        Manager $fractalManager,
        ValidatorInterface $validator
    ) {
        $client = new User();
        $mappingContext = $mappingContextFactory->create(User::class, ['client.post']);
        $propertyMapper->map($client, $request->request->all(), $mappingContext);
        /** @var Company $company */
        $company = $systemUser->getCompany();
        if (!$company) {
            throw new HttpException(500);
        }
        $client->setCompany($company);
        $client->setPlainPassword($inviteService->getRandomString());
        $client->setActivationCode($inviteService->generateActivationCode());
        $client->setIsClient(true);
        $client->addRole(User::ROLE_CLIENT);
        $departmentId = (int)$client->getDepartmentId();
        $entityManager = $this->getDoctrine()->getManager();
        /** @var DepartmentRepository $departmentRepository */
        $departmentRepository = $entityManager->getRepository('App:Department');
        $client->setDepartment($departmentRepository->find($departmentId));
        $violations = $validator->validate($client, null, ['client.post']);
        if (count($violations)) {
            throw new ValidationFailedException($violations);
        }

        $this->userManager->updateUser($client);
        $this->eventDispatcher->dispatch(EventList::USER_CREATED, new UserCreated($client->getId()));

        $fractalManager->parseIncludes('department');
        $resource = new \League\Fractal\Resource\Item(
            $client,
            $clientTransformer,
            User::JSON_API_TYPE_CLIENT
        );

        return $fractalManager->createData($resource)->toArray();
    }

    /**
     * @Rest\Post("/clients/public-registration")
     *
     * @SWG\Post(
     *      summary="Api for a client registration by themselves",
     *      tags={"Client"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          @Model(type=App\Entity\User::class, groups={"client.post.public_registration"})
     *      ),
     *      @SWG\Response(
     *          response="200",
     *          description="Returned when successful"
     *      )
     * )
     *
     * @throws ValidationFailedException
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     * @param Request $request
     * @param JmsMappingContextFactory $mappingContextFactory
     * @param PropertyMapper $propertyMapper
     * @param ClientTransformer $clientTransformer
     * @param Manager $fractalManager
     * @param ValidatorInterface $validator
     * @param InviteService $inviteService
     *
     * @return array
     */
    public function postPublicRegistration(
        Request $request,
        ClientTransformer $clientTransformer,
        Manager $fractalManager,
        ValidatorInterface $validator,
        InviteService $inviteService
    ) {
        $client = new User();

        $data = $request->request->all();

        $violations = $validator->validate(
            $data,
            new ClientPublicRegistration(
                [
                    'user' => $client,
                    'clientIp' => $request->getClientIp(),
                ]
            )
        );
        if (count($violations)) {
            throw new ValidationFailedException($violations);
        }
        $client->setUsername($client->getEmail());
        $client->setPlainPassword($inviteService->getRandomString());
        $client->setActivationCode($inviteService->generateActivationCode());
        $client->setActivationCodeType(User::HASH_TYPE_CLIENT_PUBLIC_REGISTRATION);
        $client->setIsClient(true);
        $client->setEnabled(false);
        $client->setRoles([User::ROLE_CLIENT]);

        $this->userManager->updateUser($client);
        $this->eventDispatcher->dispatch(EventList::USER_CREATED, new UserCreated($client->getId()));

        $resource = new \League\Fractal\Resource\Item(
            $client,
            $clientTransformer,
            User::JSON_API_TYPE_CLIENT
        );

        return $fractalManager->createData($resource)->toArray();
    }

    /**
     * @Rest\Put("/clients/{id}")
     *
     * @SWG\Put(
     *      summary="Put The Client",
     *      description="<b>Field department_id</b>: can be null or a number",
     *      tags={"Client"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          @Model(type=App\Entity\User::class, groups={"client.put"})
     *      ),
     *      @SWG\Response(
     *          response="200",
     *          description="Returned when successful",
     *      )
     * )
     *
     * @throws ValidationFailedException
     *
     * @param $id
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ClientTransformer $clientTransformer
     * @param Manager $fractalManager
     * @param JmsMappingContextFactory $mappingContextFactory
     * @param PropertyMapper $propertyMapper
     *
     * @return array
     */
    public function putClient(
        $id,
        Request $request,
        ValidatorInterface $validator,
        ClientTransformer $clientTransformer,
        Manager $fractalManager,
        JmsMappingContextFactory $mappingContextFactory,
        PropertyMapper $propertyMapper
    ) {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var UserRepository $userRepository */
        $userRepository = $entityManager->getRepository('App:User');

        /** @var User $user */
        $user = $userRepository->getClient($id);
        if (!$user) {
            throw new NotFoundHttpException('Client is not found');
        }
        $mappingContext = $mappingContextFactory->create(User::class, ['client.put']);
        $propertyMapper->map($user, $request->request->all(), $mappingContext);

        /** @var DepartmentRepository $departmentRepository */
        $departmentRepository = $entityManager->getRepository('App:Department');
        $department = $user->getDepartmentId() !== 0 ? $departmentRepository->find((int)$user->getDepartmentId()): null;
        $user->setDepartment($department);

        /**
         * Disabling filters is required.
         */
        $entityManager->getFilters()->disable('user_filter');
        $errors = $validator->validate($user, null, ['client.put']);
        $entityManager->getFilters()->enable('user_filter');

        if (count($errors)) {
            /**
             * Don't the code bellow, otherwise the security token will be saved with the changed data
             * to the session and later there will be the 401 http error.
             */
            $entityManager->refresh($user);
            throw new ValidationFailedException($errors);
        }

        $this->userManager->updateUser($user);

        $fractalManager->parseIncludes('department');
        $resource = new \League\Fractal\Resource\Item(
            $user,
            $clientTransformer,
            User::JSON_API_TYPE_CLIENT
        );

        return $fractalManager->createData($resource)->toArray();
    }

    /**
     * @Rest\Patch("/client-by-hash/{hash}/activate", name="api_clients_activation_put_data")
     *
     * @SWG\Patch(
     *      summary="Changes client whom was identified by hash",
     *      tags={"Client"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          @Model(type=App\Entity\User::class, groups={"client_activation.patch"})
     *      ),
     *      @SWG\Response(
     *          response="200",
     *          description="Returned when successful",
     *      )
     * )
     *
     * @throws ValidationFailedException
     * @throws \Exception
     *
     * @param $hash
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ClientTransformer $clientTransformer
     * @param Manager $fractalManager
     * @param JmsMappingContextFactory $mappingContextFactory
     * @param PropertyMapper $mapper
     *
     * @return array
     */
    public function patchClientActivation(
        $hash,
        Request $request,
        ValidatorInterface $validator,
        ClientTransformer $clientTransformer,
        Manager $fractalManager,
        JmsMappingContextFactory $mappingContextFactory,
        PropertyMapper $mapper
    ) {
        /** @var EntityRepository $repository */
        $entityManager = $this->getDoctrine()->getManager();
        /** @var UserRepository $userRepository */
        $userRepository = $entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->getUserByHash($hash, User::HASH_TYPE_CLIENT_PUBLIC_REGISTRATION);
        if (!$user || !$user->getIsClient()) {
            throw new NotFoundHttpException('Client is not found');
        }
        $mappingContext = $mappingContextFactory->create(User::class, ['client_activation.patch']);
        $mappingContext->setSetNullIfPropertyNotExist(true);
        $mapper->map($user, $request->request->all(), $mappingContext);

        $violations = $validator->validate($user, null, ['client_activation.patch']);
        if (count($violations)) {
            throw new ValidationFailedException($violations);
        }

        $user->setEnabled(true);
        $user->setActivationCode(null);
        $user->setActivationCodeType(null);

        $company = new Company();
        $company->setName($user->getCompanyName());
        $user->setCompany($company);
        $entityManager->persist($company);

        $this->userManager->updateCanonicalFields($user);
        $this->userManager->updatePassword($user);
        $entityManager->flush();
        $this->eventDispatcher->dispatch(EventList::USER_ACTIVATED, new UserActivated($user->getId()));

        $resource = new \League\Fractal\Resource\Item(
            $user,
            $clientTransformer,
            User::JSON_API_TYPE_CLIENT
        );

        return $fractalManager->createData($resource)->toArray();
    }

    /**
     * @return string
     */
    private function getDefaultSort()
    {
        return '+id';
    }

    /**
     * @return array
     */
    private function getSortingFields()
    {
        return [
            'id' => ['+', '-'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getQueryValidationConfig(): array
    {
        $getClient = new Collection([
            'include' => new Optional(),
        ]);
        $getClients = new Collection([
            'page' => new Optional(new Regex(['pattern' => '/^\d+$/'])),
            'limit' => new Optional(new Regex(['pattern' => '/^\d+$/'])),
            'company_name' => new Optional(
                new Collection([
                    'like' => new Optional([new Type(['type' => 'string']), new Length(['min' => 2, "max" => 180])])
                ])
            ),
            'first_name' => new Optional(
                new Collection([
                    'like' => new Optional([new Type(['type' => 'string']), new Length(['min' => 2, "max" => 180])])
                ])
            ),
            'last_name' => new Optional(
                new Collection([
                    'like' => new Optional([new Type(['type' => 'string']), new Length(['min' => 2, "max" => 180])])
                ])
            ),
            'email' => new Optional(
                new Collection([
                    'like' => new Optional([new Type(['type' => 'string']), new Length(['min' => 2, "max" => 180])])
                ])
            ),
            'enabled' => new Optional(new Range(['min' => 0, 'max' => 1])),
            'industry_ids' => new Optional(new Regex(['pattern' => '/^\d+(,\d+)*$/'])),
            'include' => new Optional(),
        ]);

        return [
            'getClient' => $getClient,
            'getStatistics' => false,
            'getClients' => $getClients,
            'postClient' => false,
            'postPublicRegistration' => false,
            'putClient' => false,
            'patchClientActivation' => false,
        ];
    }
}
